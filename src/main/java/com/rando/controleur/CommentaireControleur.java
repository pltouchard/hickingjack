package com.rando.controleur;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.rando.dao.EtapeRepository;
import com.rando.modele.Commentaire;
import com.rando.modele.dtos.CommentaireDto;
import com.rando.service.CommentaireService;

@Controller
public class CommentaireControleur {
	
	@Autowired
    CommentaireService commentaireService;
	
	@Autowired
	EtapeRepository etapeRepository;
	
	@GetMapping("/commentaires/{id}")
	public String commenter(Model model, @PathVariable int id) {
		
		List<Commentaire> commentaires = new ArrayList<Commentaire>();
	       
		commentaireService.getAllCommentsById(id).forEach(commentaires::add);
       
        model.addAttribute("commentaires", commentaires);
				
		return "detailEtape";
	}
	
	@GetMapping(path = "/commentaires")
	public String displayForm(Model model, @ModelAttribute Commentaire commentaire) {
	    
	    model.addAttribute("commentaire", commentaire);
	    return "commentaire";
	}
	
	@PostMapping(path = "/commentaires")
    public String processForm(@ModelAttribute("ajoutCommentaire") @Valid CommentaireDto commentaire, BindingResult result, HttpServletRequest request, RedirectAttributes redirectAttributes, Model model) {
		
		if(!result.hasErrors()) {
			commentaireService.addCommentaire(commentaire);
		}else {
			redirectAttributes.addFlashAttribute("errors", result.getAllErrors());
			
			return "redirect:/detailEtape/" + commentaire.getIdEtape();
		}
		
		
        return "redirect:/detailEtape/" + commentaire.getIdEtape();
    }

}
