package com.rando.controleur;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.rando.service.PhotoService;

@Controller
public class JspPhotoControleur { 
	
	@Autowired
	PhotoService photoService;

	@RequestMapping(value = "ajouterPhoto", method = RequestMethod.POST)
	public String uploadFileHandler(@RequestParam("file") MultipartFile file, @RequestParam("idEtape") int idEtape,
			RedirectAttributes redirectAttributes) {
		String ext = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
		System.out.println(ext);
		if (!file.isEmpty() && (ext.equals("jpg") || ext.equals("png"))) {
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = System.getProperty("catalina.home");
				File dir = new File(rootPath + File.separator + "/photos");
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath() + File.separator + file.getOriginalFilename());

				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();
				System.out.println(dir);
				System.out.println(serverFile);

				photoService.savePhotosWithEtape(dir + "/" + file.getOriginalFilename(), idEtape);
			} catch (Exception e) {
				return "You failed to upload " + file.getName() + " => " + e.getMessage();
			}
		} else {
			redirectAttributes.addFlashAttribute("error",
					"Aucune image sélectionnée / type d'image non pris en charge (uniquement JPG et PNG)");

			return "redirect:/detailEtape/" + idEtape;
		}

		return "redirect:/detailEtape/" + idEtape;
	}
}
